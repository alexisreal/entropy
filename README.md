# entropy

Test de entropy


Crear cluster Kubernetes

	Seleccionar region
		
* Considerar zonas de replicación

Tres nodos de preferencia por el requerimento de máximo uptime
		
* En el caso de los K8s en GCP el control y orquestación que haría el maestro, lo provee el propio servicio de google. 
		

Seleccionar capacidad de los nodos
	En caso de requerir un sistema de pruebas/QA/UAT o canary, se podría considerar un nodo más o un namespace aparte que haga uso de los mismos nodos originales si la carga lo permite
Se debe seleccionar el registry que se ocupará, por ejemplo el default de docker o el interno de GCP

Ya que se consideran uptimes asintóticos a 100, debemos agregar balanceadores de carga que expondrán los puertos de los pods con las tareas de servir las api móviles y web y que por supuesto requieren dos o más pods

Se considera el uso de un sistema pub/sub que pueda ser provisto por el cloud service, para suscribir los microservicios a las colas de mensajes, o en le caso de requerir encoladores propio, agregar instancias en cluster como RabbitMQ, KubeMQ, etc.

En caso de usar el servicio pub/sub propio del cloud, agregar a la vnet de kubernetes, el servicio administrado o incluir un gateway con interfaz en la vnet del cluster y poder comunicar los servicios con el exterior

Las reglas de creación de contenedores, considerará que por cada instancia consumidora, se creará una proveedora y la regla será dinámida, en el caso de desaparecer el consumidor, se detendrá una proveedora

En el git donde se haga el control de versiones, se deberá agregar el yaml que contenga las reglas de integración y despliegue para cada tipo de contenedor, ya que son diferentes bases y lenguajes para cada uno de ellos.
	* La imagen base en el registro, donde se montará el sistema
* 	El código fuente y todas sus dependencias
* 	Las reglas y scripts para construir los paquetes
* Las dependencias de paquetes de sistema
* Los archivos de configuración individuales o comunes y las rutas a directorios y variables de entorno
* El almacenamiento en contenedores de storage o en red en el caso de no ser interno
* Contraseñas, ip destino, endpoints, tokens, id, etc, para la correcta integración

Se debe crear un pipe de despliegue, para que al hacer los builds de imagen, se cargue esta en el registro privado, se agreguen las pruebas básicas unitarias o de integración con bases o componentes preexistentes.
	Se pueda desplegar en un namespace o cluster de pruebas y en caso de fallar estas, se notifique al desarrollador y se corrijan antes de hacer el roll a producción.

El auto escalamiento por métricas ad hoc como la cantidad de request por segundo, se puede considerar desde notificar desde los propios contenedores al servicio de métricas de kubernetes por medio de un Advisor en los nodos como cAdvisor, o apis colectoras externas como Prometheus y establecer las reglas adecuadas de escalamiento horizontal.

El advisor podría suscribirse a la cola de mensajes y en caso de que un nuevo microservicio, necesite un consumidor adicional, se notifique al orquestador y lo lance al cluster.

La gestión de contraseñas e identidades se puede hacer desde el servicio IAM, ya sea para controlar el despliegue mismo o el acceso a recursos por rol. Dentro del cluster Kubernetes la administración de secretos se puede lograr usando el KMS que permite almacenar contraseñas en un keyring y hacer uso de ellas desde los pods o servicios propios del K8
